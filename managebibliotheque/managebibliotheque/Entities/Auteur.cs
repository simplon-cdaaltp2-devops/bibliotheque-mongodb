﻿using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;

namespace managebibliotheque.Entities
{
    public class Auteur
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string? Id { get; set; }

        public string Nom { get; set; }

        public string Biographie { get; set; }

        public DateTime DateNaissance { get; set; }

        [BsonRepresentation(BsonType.ObjectId)]
        public List<string> Oeuvres { get; set; } = null!;

        /* [BsonIgnore]
        public List<Livre> OeuvreList { get; set; } = null!; */
    }
}
