﻿using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;

namespace managebibliotheque.Entities
{
    public class Livre
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string? Id { get; set; }

        public required string Titre { get; set; } = null!;

        [BsonRepresentation(BsonType.ObjectId)]
        public required string Auteur { get; set; } = null!;

        public required string Categorie { get; set; } = null!;

        public required string ISBN { get; set; } = null!;

        public required bool Disponibilite { get; set; }

        public required DateTime DatePublication { get; set; }
    }
}
