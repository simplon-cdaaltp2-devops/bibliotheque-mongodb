﻿using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;

namespace managebibliotheque.Entities
{
    public class Emprunteur
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string? Id { get; set; }

        public required string Nom { get; set; }

        public required string Adresse { get; set; }

        public required string NumeroTelephone { get; set; }

        [BsonRepresentation(BsonType.ObjectId)]
        public List<string> LivresEmpruntes { get; set; } = null!;

        [BsonIgnore]
        public List<Livre> LivreEmprunteList { get; set; } = null!;

    }
}
