﻿namespace managebibliotheque.Configurations
{
    public class MongoDBSettings
    {
        /// <summary>
        /// La chaine de connexion au serveur de base de données MongoDB
        /// </summary>
        /// <value>
        /// The connection URI.
        /// </value>
        public string ConnectionURI { get; set; } = null!;

        /// <summary>
        /// Le nom de la base de données MongoDB
        /// </summary>
        /// <value>
        /// The name of the database.
        /// </value>
        public string DatabaseName { get; set; } = null!;

        /// <summary>
        /// La collection contenant les informations des livres
        /// </summary>
        /// <value>
        /// The name of the collection.
        /// </value>
        public string BooksCollectionName { get; set; } = null!;

        /// <summary>
        /// La collection contenant les informations des auteurs
        /// </summary>
        /// <value>
        /// The name of the collection.
        /// </value>
        public string AuthorsCollectionName { get; set; } = null!;
    }
}
