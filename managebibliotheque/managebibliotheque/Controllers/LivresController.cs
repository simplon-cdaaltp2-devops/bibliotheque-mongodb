﻿using AutoMapper;
using managebibliotheque.Entities;
using managebibliotheque.Models;
using managebibliotheque.Services.Contracts;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;

namespace managebibliotheque.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LivresController : ControllerBase
    {
        private readonly ILivreService _livreService;
        private readonly IAuteurService _auteurService;
        private readonly IMapper _mapper;

        public LivresController(ILivreService livreService, IAuteurService auteurService, IMapper mapper)
        {
            _livreService = livreService;
            _auteurService = auteurService;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Livre>>> Get()
        {
            var livres = await _livreService.GetAllBookAsync();
            return Ok(livres);
        }

        [HttpGet("{id}:length(24)")]
        public async Task<ActionResult<ReadLivre>> Get(string id)
        {
            var livre = await _livreService.GetBookByIdAsync(id);

            if(livre is null)
            {
                return NotFound();
            }

            var auteur = await _auteurService.GetAuthorByIdAsync(livre.Auteur);
            ReadLivre readLivre = _mapper.Map<ReadLivre>(livre);
            readLivre.AuteurValue = auteur;
            return Ok(readLivre);
        }

        [HttpPost]
        public async Task<ActionResult<Livre>> Post([FromBody] Livre livre)
        {
            var addedLivre = await _livreService.AddBookAsync(livre);

            if(addedLivre is null)
            {
                return Problem("Echec de création d'un livre");
            }

            return CreatedAtAction(nameof(Get), new { id = addedLivre.Id }, addedLivre);
        }

        [HttpPut("{id}:length(24)")]
        public async Task<IActionResult> Put(string id, [FromBody] Livre livre)
        {
            var checkBook = await _livreService.GetBookByIdAsync(id);

            if (checkBook is null)
            {
                return NotFound("Pas de livre avec cet identifiant");
            }

            livre.Id = checkBook.Id;
            var response =  await _livreService.UpdateBookAsync(id, livre);

            return Ok(response);
        }

        [HttpDelete("{id}:length(24)")]
        public async Task<IActionResult> Delete(string id)
        {
            var checkBook = await _livreService.GetBookByIdAsync(id);

            if (checkBook is null)
            {
                return NotFound("Pas de livre avec cet identifiant");
            }

            var response = await _livreService.RemoveBookByIdAsync(id);

            return Ok(response);
        }
    }
}
