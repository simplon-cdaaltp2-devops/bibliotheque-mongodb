using managebibliotheque.Configurations;
using managebibliotheque.Services.Contracts;
using managebibliotheque.Services.Implementations;
using MongoDB.Driver;

var builder = WebApplication.CreateBuilder(args);

// Configuration d'AutoMapper
builder.Services.AddAutoMapper(typeof(Program));

// Recupération des paramètres
builder.Services.Configure<MongoDBSettings>(builder.Configuration.GetSection("MongoDBSettings"));

// Ajoutez la configuration MongoDB
var mongoConnectionString = builder.Configuration.GetSection("MongoDBSettings:ConnectionURI")?.Value;
builder.Services.AddSingleton<IMongoClient>(new MongoClient(mongoConnectionString));

// Ajoutez les services
builder.Services.AddScoped<ILivreService, LivreService>();
builder.Services.AddScoped<IAuteurService, AuteurService>();



// Ajoutez les contrôleurs
builder.Services.AddControllers();

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
