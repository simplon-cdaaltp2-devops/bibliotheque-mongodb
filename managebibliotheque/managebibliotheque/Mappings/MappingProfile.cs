﻿using AutoMapper;
using managebibliotheque.Entities;
using managebibliotheque.Models;

namespace managebibliotheque.Mappings
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Livre, ReadLivre>();
        }
    }
}
