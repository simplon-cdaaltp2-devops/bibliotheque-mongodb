﻿using managebibliotheque.Entities;
using MongoDB.Bson;
using MongoDB.Driver;

namespace managebibliotheque.Services.Contracts
{
    public interface ILivreService
    {
        Task<Livre> GetBookByIdAsync(string id);
        Task<IEnumerable<Livre>> GetAllBookAsync();
        Task<Livre> AddBookAsync(Livre entity);
        Task<ReplaceOneResult> UpdateBookAsync(string id, Livre entity);
        Task<DeleteResult> RemoveBookByIdAsync(string id);
    }
}
