﻿using managebibliotheque.Entities;

namespace managebibliotheque.Services.Contracts
{
    public interface IAuteurService
    {
        Task<Auteur> GetAuthorByIdAsync(string id);
    }
}
