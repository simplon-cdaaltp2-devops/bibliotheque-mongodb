﻿using managebibliotheque.Configurations;
using managebibliotheque.Entities;
using managebibliotheque.Services.Contracts;
using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace managebibliotheque.Services.Implementations
{
    public class AuteurService : IAuteurService
    {
        private readonly IMongoCollection<Auteur> _authorCollection;
        public AuteurService(IOptions<MongoDBSettings> options, IMongoClient client)
        {
            var database = client.GetDatabase(options.Value.DatabaseName);
            _authorCollection = database.GetCollection<Auteur>(options.Value.AuthorsCollectionName);
        }

        public async Task<Auteur> GetAuthorByIdAsync(string id)
        {
            return await _authorCollection.Find(s => s.Id == id).FirstOrDefaultAsync();
        }
    }
}
