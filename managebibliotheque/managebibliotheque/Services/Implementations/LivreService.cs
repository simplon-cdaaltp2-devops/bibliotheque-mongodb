﻿using managebibliotheque.Configurations;
using managebibliotheque.Entities;
using managebibliotheque.Services.Contracts;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;

namespace managebibliotheque.Services.Implementations
{
    public class LivreService : ILivreService
    {
        private readonly IMongoCollection<Livre> _bookCollection;
        public LivreService(IOptions<MongoDBSettings> options, IMongoClient client)
        {
            var database = client.GetDatabase(options.Value.DatabaseName);
            _bookCollection = database.GetCollection<Livre>(options.Value.BooksCollectionName);
        }

        public async Task<Livre> AddBookAsync(Livre entity)
        {
            await _bookCollection.InsertOneAsync(entity);
            return entity;
        }

        public async Task<IEnumerable<Livre>> GetAllBookAsync()
        {
            return await _bookCollection.Find(s => true).ToListAsync();
        }

        public async Task<Livre> GetBookByIdAsync(string id)
        {
            return await _bookCollection.Find(s => s.Id == id).FirstOrDefaultAsync();
        }

        public async Task<DeleteResult> RemoveBookByIdAsync(string id)
        {
            return await _bookCollection.DeleteOneAsync(s => s.Id == id);
        }

        public async Task<ReplaceOneResult> UpdateBookAsync(string id, Livre entity)
        {
            return await _bookCollection.ReplaceOneAsync(s => s.Id == id, entity);
        }
    }
}
