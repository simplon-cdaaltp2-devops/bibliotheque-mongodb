﻿using managebibliotheque.Entities;
using MongoDB.Bson.Serialization.Attributes;

namespace managebibliotheque.Models
{
    public class ReadLivre: Livre
    {
        [BsonIgnore]
        public Auteur AuteurValue { get; set; } = null!;
    }
}
